const {describe} = require('mocha');
const {expect} = require('chai');
var R = require("../src/routes/routes.js");

describe('Simple test suite (with chai):', function() {
    it('1 === 1 should be true', function() {
        expect(1).to.equal(1);
    });

    it('add10(1) should be 11', function() {
        expect(R.add10(1)).to.equal(11);
    });

    it('helloStr() should return Ayla', function() {
        expect(R.helloStr()).to.equal("Friends! This is node.");
    });
});
