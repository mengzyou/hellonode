var appRouter = function(app) {
  app.get("/", function(req, res) {
    var msg = "Hello, " + helloStr();
    res.send(msg);
  });

  app.get("/add10", function(req, res) {
    if (!req.query.value) {
      return res.send({"status": "error", "message": "missing value"});
    } else {
      var ret = {
        "value": add10(parseInt(req.query.value))
      }
      return res.send(ret);
    }
  });

  app.get("/account", function(req, res) {
    var accountMock = {
      "username": "mengzyou",
      "password": "1234",
      "twitter": "@mengzyou"
    }

    if (!req.query.username) {
      return res.send({"status": "error", "message": "missing username"});
    } else if (req.query.username != accountMock.username) {
      return res.send({"status": "error", "message": "wrong username"});
    } else {
      return res.send(accountMock);
    }
  });

  app.post("/account", function(req, res) {
    if (!req.body.username || !req.body.password || !req.body.twitter) {
      return res.send({"status": "error", "message": "missing a parameter"});
    } else {
      return res.send(req.body);
    }
  });

}

function add10(a) {
  return a + 10;
}

function helloStr() {
    return "Friends! This is NodeJS!"
}

module.exports = {
  appRouter,
  add10,
  helloStr
};
